<header id="header">
				<div class="container">
					<div class="row primary-header">
						<a href="index.php" class="col-xs-12 col-sm-3 brand" title="St. Thomas More Unilag"><img src="assets/img/logo.png" alt="STMunilag"></a>
						<div class="social-links col-xs-12 col-sm-9">
							<!--<a href="volunteer.php" class="btn btn-default btn-volunteer">Become volunteer</a>-->
                           
							<ul class="social-icons hidden-xs">
								<li>
									<a href="http://facebook.com" target="_blank"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="http://plus.google.com" target="_blank"><i class="fa fa-google-plus"></i></a>
								</li>
								<li>
									<a href="http://twitter.com" target="_blank"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="http://youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a>
								</li>
								
							</ul>
                           
						</div>
					</div>
				</div>
				<div class="navbar navbar-default" role="navigation">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<nav>
							<ul class="nav navbar-nav">
								<li class="active">
									<a href="index.php">Home  </a>
								</li>

								<li>
									<a href="javascript:;"  data-toggle="dropdown"  class="submenu-icon">About Us <span class="glyphicon glyphicon-chevron-down"></span> <span class="glyphicon glyphicon-chevron-up"></span> </a>
									<!-- Drop Down  -->

									<div  class="dropdown-menu">
										<ul>
                                        	<li>
												<a href="our-story.php">The Chaplaincy</a>
													<div  class="dropdown-menu">
														<ul>
															<li>
																<a href="event-calendar.php">Calendar</a>
															</li>
															<li>
																<a href="event-calendar.php">History</a>
															</li>
															<li>
																<a href="event-calendar.php">Chaplaincy Activities</a>
															</li>
														</ul>
													</div>
											</li>
											
										<li>
											<a href="javascript:;" data-toggle="dropdown" class="submenu-icon">Societies <span class="glyphicon glyphicon-chevron-down"></span> <span class="glyphicon glyphicon-chevron-up"></span> </a>
											<!-- Drop Down  -->
											<div  class="dropdown-menu">
												<ul>
													<li>
														<a href="nfcs.php">Legion of Mary</a>
													</li>
													<li>
														<a href="nfcs.php">Bible Study</a>
													</li>
													<li>
														<a href="nfcs.php">Charismatic</a>
													</li>
													<li>
														<a href="nfcs.php">Saint Egidio</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="dropdown dropdown-submenu">
												<a  data-toggle="dropdown" class="submenu-icon">Liturgical Groups <span class="glyphicon glyphicon-chevron-down"></span> <span class="glyphicon glyphicon-chevron-up"></span> </a>
												<!-- Drop Down  -->
													<ul class="dropdown-menu">
														<li>
															<a href="nfcs.php">Choir</a>
														</li>
														<li>
															<a href="nfcs.php">Board of Lectors</a>
														</li>
														<li>
															<a href="nfcs.php">Church Wardens</a>
														</li>
														
													</ul>
												
												<!-- end  -->

										</li>
                                            <li>
												<a href="patron-saint.php">Our Patron Saint</a>
											</li>
                                            <li>
												<a href="tour.php">Tour</a>
											</li>
										</ul>
									</div>
								</li>
								
                                <li>
									<a href="gallery.php">Gallery</a>
								</li>
								<li>
									<a href="contact-us.php">contact us</a>
								</li>

							</ul>
							</nav>
							
							<form class="navbar-form navbar-right search-form" role="search">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Search Here">
								</div>
								<button type="submit">
									<i class="icon-search"></i>
								</button>
							</form>
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
				</div>

			</header>
			