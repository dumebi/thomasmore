<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>Charity</title>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<!-- google fonts -->
		<link href='http://fonts.googleapis.com/css?family=Lato:400,300italic,300,700%7CPlayfair+Display:400,700italic%7CRoboto:300%7CMontserrat:400,700%7COpen+Sans:400,300%7CLibre+Baskerville:400,400italic' rel='stylesheet' type='text/css'>
		<!-- Bootstrap -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="assets/css/bootstrap-theme.css" rel="stylesheet">
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
		<link href="assets/revolution-slider/css/settings.css" rel="stylesheet">
		<link href="assets/css/global.css" rel="stylesheet">
		<link href="assets/css/style.css" rel="stylesheet">
		<link href="assets/css/responsive.css" rel="stylesheet">
		<link href="assets/css/skin.css" rel="stylesheet">
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body>

		<div id="wrapper">
				<!--Header Section Start Here -->
			<?php
			include_once ("includes/header.php");
			?>
			<!-- Header Section End Here -->
			<!-- site content -->
			<div id="main">
			<!-- Breadcrumb Section Start Here -->
				<div class="breadcrumb-section">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<h1>Portfolio </h1>
								<ul class="breadcrumb">
									<li>
										<a href="index.php">Home</a>
									</li>
									<li class="active">
										Portfolio 
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			<!-- Breadcrumb Section End Here -->
				<div class="content-wrapper" id="page-info">

					<!-- portfolio detail sections -->
					<div class="container">
						<div class="row">
							<div class="col-xs-12">

								<header class="page-header section-header text-left">
									<h2>Portfolio Heading Title Here</h2>
								</header>
								
								<section class="img-slider flex-slide flexslider">
									<ul class="slides">
									<li>
										<img src="assets/img/blog-pic0.jpg" alt="">
									</li>
									<li>
										<img src="assets/img/blog-pic1.jpg" alt="">
									</li>
									<li>
										<img src="assets/img/blog-pic3.jpg" alt="">
									</li>
									</ul>
								</section>
									
									<div class="row we-help anim-section">
											<div class="cols-xs-12 zoom col-sm-4">
												<div class="thumbnail">
													<a href="portfolio-detail.php" class="img-thumb"><img alt="" src="assets/img/portfolio1.jpg"></a>

												</div>
											</div>

											<div class="cols-xs-12 zoom col-sm-4">
												<div class="thumbnail">
													<a href="portfolio-detail.php" class="img-thumb"><img alt="" src="assets/img/portfolio2.jpg"></a>
												</div>
											</div>

											<div class="cols-xs-12 zoom col-sm-4">
												<div class="thumbnail">
													<a href="portfolio-detail.php" class="img-thumb"><img alt="" src="assets/img/portfolio3.jpg"></a>
												</div>
											</div>
										</div>

							</div>
						</div>
					</div>
					<!--  sections end -->

				</div>

			</div>
			</div>
			</div>
			<!-- site content ends -->

			<!--Footer Section Start Here -->
			<?php
			include_once ("includes/footer.php");
			?>
			<!--Footer Section End Here -->
		</div>
		<script src="assets/js/jquery.min.js"></script>
		<!-- Switcher Style End Js -->
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/site.js"></script>
		<!-- Bootstrap Js--> 
		<script src="assets/js/jquery.easing.min.js"></script>
		<!--Main Slider Js-->
		<script src="assets/revolution-slider/js/jquery.themepunch.plugins.min.js"></script>
		<script src="assets/revolution-slider/js/jquery.themepunch.revolution.js"></script>
		<!--Main Slider Js End -->
		<script src="assets/js/jquery.flexslider.js"></script>
		<script src="assets/js/site.js"></script>
	</body>
</html>